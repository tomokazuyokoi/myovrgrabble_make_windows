﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyRightController : MonoBehaviour
{
    OVRInput.Controller rightController;    // コントローラ_右手
    bool isTrigger = false;                 // コントローラ_IndexTriggerのオンオフ
    [SerializeField] Transform gripTrans;   // コントローラ_右手の掴む位置
    GameObject grabbedObj;                  // 掴んだオブジェクト

    void Start()
    {
        // コントローラ_右手の取得
        rightController = OVRInput.Controller.RTouch;
    }

    void Update()
    {
        //-------------------------------------
        // IndexTriggerのオンオフ
        //-------------------------------------
        // IndexTriggerのオン
        // ・isTriggerオン
        // ・掴んだオブジェクトと手の位置をリンク
        // ・掴んだオブジェクトの物理演算オフ
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, rightController) > 0.5)
        {
            isTrigger = true;
            if (grabbedObj != null)
            {
                grabbedObj.transform.position = gripTrans.position;
                grabbedObj.transform.rotation = gripTrans.rotation;
                grabbedObj.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        // IndexTriggerのオフ
        // ・isTriggerオフ
        // ・掴んだオブジェクトの物理演算オン
        // ・掴んだオブジェクトに力を加える
        // ・掴んだオブジェクトの破棄
        else
        {
            isTrigger = false;
            if (grabbedObj != null)
            {
                grabbedObj.GetComponent<Rigidbody>().isKinematic = false;
                grabbedObj.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(rightController);
                grabbedObj.GetComponent<Rigidbody>().angularVelocity = OVRInput.GetLocalControllerAngularVelocity(rightController);
                grabbedObj = null;
            }
        }
    }

    /**
     * コントローラとcubeが接触した時
     */
    private void OnTriggerStay(Collider other)
    {
        // コントローラ_IndexTriggerを押した時
        if (isTrigger && other.tag == "Gravabble" && !grabbedObj)
        {
            // cubeオブジェクトを取得
            grabbedObj = other.gameObject;
        }
    }
}
